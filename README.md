# Nicolas-dmg - Logger

A simple logger built with chalk.

## Installation

```bash
npm i @nicolas-dmg/logger
```

## Usage

```js
import logger from '@nicolas-dmg/logger';

logger.error("Your message");
```

## About me

<table style="border: none;">
  <tr>
    <td>
      <div style="width: 120px;">
        <img width="120" src="https://avatars1.githubusercontent.com/u/46563166?s=460&u=8d851cf38c28b0f78cbacdccaa9f332e73687f52&v=4"/>
    </div>
    </td>
    <td>
      <div style="margin-left: 30px;">
        <p>Hey there !</br>
        I am Nicolas, Software engineer <a href="https://mailjet.com/">@mailjet</a> the day, side projects the night.
        </br>
        If you have any question you can <a href="http://go.nicolas-dmg.fr/in">contact me</a> 😃.</p>
        <p>I'm always ready to help !</p>
        <a href="mailto:contact@nicolas-dmg.fr?subject=Hey! Are you available?">Email me</a>
        &nbsp;
        <a href="http://go.nicolas-dmg.fr/dev" rel="noopener noreferrer" target="_blank">Visit my website</a>
        &nbsp;
        <a href="http://go.nicolas-dmg.fr/in" rel="noopener noreferrer" target="_blank">Connect with me</a>
    </div>
    </td>
  </tr>
</table>

---

## License

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

- **[MIT license](http://opensource.org/licenses/mit-license.php)**
- Copyright 2019-2023 © <a href="https://nicolas-dmg.fr/" target="_blank">Nicolas Dommanget</a>.

