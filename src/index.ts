import chalk from 'chalk';
import { ErrorResponse } from './types/ErrorResponse';

const httpError = (response: ErrorResponse, routeName: string): void => {
  console.log(
    chalk.red(
      `${chalk.white.bgRed.bold(
        ` ${response.statusCode} ${response.controllerName ? `- ${response.controllerName} ` : ''}`,
      )} ${response.message} on ${routeName}`,
    ),
  );
};

const log = (controllerName: string, ...message: any[]): void => {
  console.log(chalk.white(`${chalk.blueBright.bgWhite.bold(` LOG ${controllerName ? `- ${controllerName} ` : ''}`)}`), ...message);
};

const info = (message: any): void => {
  console.log(chalk.blueBright(`${chalk.white.bgBlueBright.bold(' INFO ')} ${message}`));
};

const error = (message: any): void => {
  console.log(chalk.red(`${chalk.white.bgRed.bold(' ERROR ')} ${message}`));
};

const logger = {
  httpError,
  log,
  info,
  error,
};

export default logger;
