export type ErrorResponse = {
    statusCode: number;

    controllerName: string;
  
    message: string;
  
    data?: unknown;
}